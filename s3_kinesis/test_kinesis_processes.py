from multiprocessing import Process
import os
import time
import random
import json
from s3_kinesis.kinesis_wrap import KinesisWrap
from utils import tm_log
logger = tm_log.get_logger()
num_recs = 1_000
num_counters = 20
stream_name = 'ittais1'


class CounterRec:
    def __init__(self, name, time_stamp, val):
        self.name = name
        self.time_stamp = time_stamp
        self.val = val


def info(title):
    logger.info(title)
    logger.info(f'module name: { __name__}')
    logger.info(f'parent process: { os.getppid()}')
    logger.info('process id: {os.getpid()}')


def counter_generator():
    logger.info('start generator')
    info('generator')
    kinesis_wrap1 = KinesisWrap()
    tot = num_counters * [0]
    last_snum = 0
    for i in range(num_recs):
        cid = random.randint(0, num_counters-1)
        val = random.randint(0, 999)
        tot[cid] += val
        rec = CounterRec(str(cid), time.time(), val)
        data = json.dumps(rec, default=lambda x: x.__dict__)
        key = str(cid)
        r = kinesis_wrap1.put_record(stream_name, data, key, last_snum)
        # logger.info(r)
        last_snum = r['SequenceNumber']
    for cid in range(num_counters-1):
        rec = CounterRec(str(cid), time.time(), -1)
        data = json.dumps(rec, default=lambda x: x.__dict__)
        key = str(cid)
        r = kinesis_wrap1.put_record(stream_name, data, key, last_snum)
        last_snum = r['SequenceNumber']

    logger.info('end put records')
    for cid in range(num_counters):
        logger.info(f'tot counter {cid}: {tot[cid]}')


def reader(shard_id, start_snum):
    logger.info(f'start reader - {shard_id}')
    info(f'reader {shard_id}')
    tot = num_counters * [0]
    kinesis_wrap1 = KinesisWrap()
    records,  iterator = kinesis_wrap1.get_records_by_snum(stream_name, shard_id, start_snum)
    logger.info(f'read {shard_id} - {len(records)}')
    rcnt = 0
    more = True
    for rec in records:
        rcnt += 1
        data = rec['Data'].decode()
        cntr = json.loads(data)
        cid = int(cntr['name'])
        val = int(cntr['val'])
        if val < 0:
            more = False
            break
        tot[cid] += val
    while more:
        time.sleep(1)
        records, iterator = kinesis_wrap1.get_records_from_iterator(iterator)
        logger.info(f'read {shard_id} - {len(records)}')
        for rec in records:
            rcnt += 1
            data = rec['Data'].decode()
            cntr = json.loads(data)
            cid = int(cntr['name'])
            val = int(cntr['val'])
            if val < 0:
                more = False
                break
            tot[cid] += val
    logger.info(f'end reader - {shard_id}')
    for cid in range(num_counters):
        logger.info(f'tot counter {cid}: {tot[cid]}')


if __name__ == '__main__':
    info('main line')
    p = Process(target=counter_generator, args=())
    kinesis_wrap0 = KinesisWrap()
    p.start()
    time.sleep(5)
    kinesis_wrap0 = KinesisWrap()
    shards = kinesis_wrap0.get_shards(stream_name)
    for shard in shards.values():
        p1 = Process(target=reader, args=(shard.shard_id, shard.start_sequence_number))
        p1.start()
