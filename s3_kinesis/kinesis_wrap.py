import logging
import boto3
import botocore
from boto import kinesis
from s3_bucket import s3_key
from utils import tm_log
logger = tm_log.get_logger()
for name in logging.Logger.manager.loggerDict.keys():
    if ('boto' in name) or ('urllib3' in name):
        logging.getLogger(name).setLevel(logging.WARNING)

class Shard:
    def __init__(self, shard_id, start_sequence_number):
        self.shard_id = shard_id
        self.start_sequence_number = start_sequence_number


class KinesisWrap:
    def __init__(self):
        try:
            self.client: kinesis.client = boto3.client('kinesis', aws_access_key_id=s3_key.get_access_id(),
                                                    aws_secret_access_key=s3_key.get_access_key())
        except botocore.exceptions.ClientError as e:
            raise

    def get_shards(self, stream_name):
        jr = self.client.list_shards(StreamName=stream_name)
        shards = {}
        for jshard in jr['Shards']:
            shard_id = jshard['ShardId']
            start_sequence_number = jshard['SequenceNumberRange']['StartingSequenceNumber']
            shard = Shard(shard_id, start_sequence_number)
            shards[shard_id] = shard

        return shards

    def put_record(self, stream_name, data, partition_key, snum):
        data_bytes = str.encode(data)
        response = self.client.put_record(StreamName=stream_name, Data=data_bytes, PartitionKey=partition_key,  SequenceNumberForOrdering=str(snum))
        return response

    def get_all_records_of_shard(self, stream_name, shard_id):
        response0 = self.client.get_shard_iterator(
            StreamName=stream_name,
            ShardId=shard_id,
            ShardIteratorType='TRIM_HORIZON'
        )
        shard_iterator = response0['ShardIterator']

        response = self.client.get_records(ShardIterator=shard_iterator, Limit=10000)
        records = response['Records']
        retry = 0
        while len(records) <= 0:
            next_shard_iterator = response['NextShardIterator']
            response = self.client.get_records(ShardIterator=next_shard_iterator, Limit=10000)
            retry += 1
            records = response['Records']
        logger.info(f'retry count: {retry}')
        return records

    def get_records_by_snum(self, stream_name, shard_id, snum):
        response0 = self.client.get_shard_iterator(
            StreamName=stream_name,
            ShardId=shard_id,
            ShardIteratorType='AT_SEQUENCE_NUMBER',
            StartingSequenceNumber=snum

        )
        shard_iterator = response0['ShardIterator']

        response = self.client.get_records(ShardIterator=shard_iterator, Limit=1000)
        records = response['Records']
        shard_iterator = response['NextShardIterator']
        return records, shard_iterator

    def get_records_after_snum(self, stream_name, shard_id, snum):
        response0 = self.client.get_shard_iterator(
            StreamName=stream_name,
            ShardId=shard_id,
            ShardIteratorType='AFTER_SEQUENCE_NUMBER',
            StartingSequenceNumber=snum
        )
        shard_iterator = response0['ShardIterator']

        response = self.client.get_records(ShardIterator=shard_iterator, Limit=1000)
        records = response['Records']
        shard_iterator = response['NetxShardIterator']
        return records, shard_iterator

    def get_records_from_iterator(self, shard_iterator):
        response = self.client.get_records(ShardIterator=shard_iterator, Limit=1000)
        records = response['Records']
        shard_iterator = response['NextShardIterator']
        return records, shard_iterator
