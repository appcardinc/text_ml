from s3_kinesis.kinesis_wrap import KinesisWrap
from utils import tm_log
logger = tm_log.get_logger()

kinesis_wrap = KinesisWrap()
stream_name = 'ittais1'
shard_start_snum = {}


def put_recs(num_records):
    last_snum = 0
    for n in range(num_records):
        data = 'record -' + str(n)
        key = 'ABCDE-' + str(n)
        r = kinesis_wrap.put_record(stream_name, data, key, last_snum)
        logger.info(r)
        shard_id =r['ShardId']
        last_snum = r['SequenceNumber']
        if shard_id not in shard_start_snum:
            shard_start_snum[shard_id] = last_snum
        logger.info(f' put record {shard_id} - {last_snum}')


def get_recs1():
    for shard_id in shard_start_snum.keys():
        records, iterator = kinesis_wrap.get_records_by_snum(stream_name, shard_id, shard_start_snum[shard_id])

        print(f' start from snum recods {shard_id} - {len(records)}')


def get_recs2():
    shards = kinesis_wrap.get_shards(stream_name)
    for shard in shards.values():
        records, iteator = kinesis_wrap.get_records_by_snum(stream_name, shard.shard_id, shard.start_sequence_number)
        for rec in records:
            data = rec['Data'].decode()
            print(data)
        print(f' start from 0{len(records)}')




#put_recs(10)
#get_recs1()
get_recs2()

