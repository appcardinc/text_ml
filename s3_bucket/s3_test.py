import json
from Text_Mining.s3_bucket import s3_data, s3_conf
from Text_Mining.utils import tm_log
logger = tm_log.init_tm_log()

s = s3_data.load_s3_file(s3_conf.s3_bucket_name , s3_conf.stop_rules)
slist = s.split('\n')
js = json.loads(slist[0])
logger.info(js)
