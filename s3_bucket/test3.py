import boto3
import botocore
from Text_Mining.s3_bucket import s3_conf, s3_key
import time
from Text_Mining.utils import tm_log

logger = tm_log.init_tm_log()

def load_s3_records(bucket_name, file_path):

    try:
        s3 = boto3.client('s3',  aws_access_key_id=s3_key.get_access_id(),
                          aws_secret_access_key=s3_key.get_access_key())
        obj = s3.get_object(Bucket=bucket_name, Key=file_path)
        byte_content = obj['Body'].read()
        str_content = byte_content.decode('utf-8')
        recs = str_content.split (('\n'))
        return recs
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == '404':
            print('file does not exist')
        else:
            # any other error just re-raise the exception
            raise


start_time = time.time()
r = load_s3_records(s3_conf.s3_bucket_name, s3_conf.tax_dir + '/' + s3_conf.wikicat)
logger.info(len(r))
for i in range(0, 100):
    logger.info(r[i])
logger.info('load wiki s3 ' + "--- %s seconds ---" % (time.time() - start_time))
