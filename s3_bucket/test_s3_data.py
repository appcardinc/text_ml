from Text_Mining.s3_bucket import s3_conf, s3_data
import time
from Text_Mining.utils import tm_log

logger = tm_log.init_tm_log()

start_time = time.time()
s3_data.download_s3_file(s3_conf.s3_bucket_name, s3_conf.tax_dir, s3_conf.super_king_cat)
logger.info('load super king cat '  + "--- %s seconds ---" % (time.time() - start_time))