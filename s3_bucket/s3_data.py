import boto3
import botocore
from Text_Mining.s3_bucket.s3_key import *


def load_s3_file(bucket_name, dir1, file_name):
    try:
        s3 = boto3.client('s3', aws_access_key_id=get_access_id(), aws_secret_access_key=get_access_key())
        file_path = dir1 + "/" + file_name
        obj = s3.get_object(Bucket=bucket_name, Key=file_path)
        byte_content = obj['Body'].read()
        str_content = byte_content.decode('utf-8')
        recs = str_content.splitlines()
        return recs
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == '404':
            print('file does not exist')
        else:
            # any other error just re-raise the exception
            raise


def download_s3_file(bucket_name, dir1, file_name):
    try:
        s3 = boto3.resource('s3', aws_access_key_id=get_access_id(), aws_secret_access_key=get_access_key())
        file_path = dir1 + "/" + file_name
        target_path = '/tmp/' + file_name
        with open(target_path, 'wb') as data:
            s3.Bucket(bucket_name).download_fileobj(file_path, data)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise
