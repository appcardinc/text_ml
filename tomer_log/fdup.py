import os
import logging
import re
import datetime

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    handlers=[logging.FileHandler("./pythonlog.txt"), logging.StreamHandler()],
)
logger = logging.getLogger("LOGDUP")

class TransLine():
    def __init__(self, time_stamp, trid,  msg_type, data, line_text):
        self.time_stamp = time_stamp
        self.trid = trid
        self.msg_type = msg_type
        self.data = data
        self.line_text = line_text
        self.dup_indx = -1


class Transaction():
    def __init__(self, time_stamp, trid):
        self.dup_type = -1
        self.start = time_stamp
        self.end = None
        self.trid = trid
        self.lines = []
        self.dup_formats = set()

    def add_line(self, line: TransLine):
        self.lines.append(line)

    def set_end(self, time_stamp):
        self.end = time_stamp

    def set_dup_format(self):
        for i1 in range(len(self.lines)-1):
            l1 = self.lines[i1]
            if l1.msg_type != "I":
                continue
            for i2 in range(i1+1, len(self.lines)):
                l2 = self.lines[i2]
                if l2.msg_type == "I":
                    if l2.data == l1.data:
                        if l2.dup_indx < 0:
                            l2.dup_indx = i1

        for i1 in range(1, len(self.lines)):
            l1 = self.lines[i1]
            if l1.dup_indx >= 0:
                dup_format = None
                if i1 >= len(self.lines)-1:
                    dup_format = 'last_line'
                elif self.lines[i1+1].msg_type == "T":
                    dup_format = '1_before_total'
                elif self.lines[i1+1].msg_type == "P":
                    dup_format = '1_before_payment'
                elif i1 < len(self.lines)-2:
                    if  self.lines[i1+1].dup_indx >= 0 and self.lines[i1+2].msg_type == "T":
                        dup_format = '2_dups_before_total'
                if dup_format is None:
                    dup_format = 'other'
                self.dup_formats.add(dup_format)

    def print(self):
        logger.info (' --------------------------------------------------------------')
        logger.info(f' Transaction: {self.trid} start: {self.start} end: {self.end}')
        for i1 in range (len(self.lines)):
            lindx = "{0:04}".format(i1)
            l1 = self.lines[i1]
            dup = (5)*' '
            if l1.dup_indx >= 0:
               dup = '*'+"{0:04}".format(l1.dup_indx)
            logger.info(f'{lindx} -  {dup} {self.lines[i1].line_text}')


open_transactions = {}
transactions = []

path_data = '/Users/ittai/Desktop/tomer/Archive'
log1 = 'apse0004_enginelog.log'
log2 = 'apse0010_enginelog.log'
c1 = 'Msg Req  :'
c2 = 'Msg Resp :'


reg_tran = re.compile('\d{2}\d*\d*\s[A-Z]')


def test_format():
    tlB = '2019-06-21 15:28:25,421  INFO MessagesHelper:150 - Msg Req  : 37  2 9311 B 0 0 \ 18644 \ 190621142954 \ 19311 \  \  \  \  \ '
    tlE = '2019-06-21 15:53:59,213  INFO MessagesHelper:150 - Msg Req  : 64  6 6439 E 0 0 ' \
          '\ 18421 \ 190621145358 \ 176439 \ 3548 \ 83 \ 0 \ 0 \ 0 \ 0 \ 0 \ 0 \ 1 \ 882 \ 0 \ 0 \ '
    tlI = '2019-06-21 15:53:56,540  INFO MessagesHelper:150 - Msg Req  : 76  2 9320 I 0 0 \ 0 \ 1 \ 0000000000601?' \
          ' \ 6 \ 1 \ 799 \ ROTISSERIE CHICKEN WHOLE \ 3176 \ 179 \ 1 \ 799 \  \ '


    m = reg_tran.search(tlB)
    print(m.start())
    print(m.group())
    time_stamp =  datetime.datetime.strptime(tlB[0:23], '%Y-%m-%d %H:%M:%S,%f')
    print (time_stamp)
    print(time_stamp.time())


def load_recs(log_name):
    path1 = os.path.join(path_data, log_name)
    logger.info(f' load.  log path: {path1}')
    rcnt = 0
    rel_cnt = 0
    no_start = 0
    dup_transaction_cnt = 0
    with open (path1,'r') as f:
        for line_text in f:
            rcnt += 1
            if line_text.find(c1) <  0 and line_text.find(c2) < 0:
                continue
            rel_cnt += 1
            time_stamp =  datetime.datetime.strptime(line_text[0:23], '%Y-%m-%d %H:%M:%S,%f')
            m = reg_tran.search(line_text)
            if m is None:
                # logger.error(f' bad line {line_text}')
                continue
            trid = int (m.group().split()[0])
            msg_type = m.group().split()[1]
            data = line_text[m.start()+7:]
            trans_line = TransLine(time_stamp, trid, msg_type, data, line_text)
            if trid in open_transactions:
                transaction = open_transactions[trid]
                transaction.add_line(trans_line)
                if msg_type == "B":
                   dup_transaction_cnt += 1
                elif msg_type == "E":
                    transaction.set_end(time_stamp)
                    open_transactions.pop(trid)
            else:
                if msg_type != "B":
                    no_start += 1
                transaction = Transaction(time_stamp, trid)
                open_transactions[trid] = transaction
                transactions.append(transaction)
                transaction.add_line(trans_line)

    logger.info(f' recs: {rcnt} rel: {rel_cnt} transactiobs {len(transactions)} no start: {no_start}'
                f' dup transaction {dup_transaction_cnt}')

    dup_cnt = {}
    for transaction in transactions:
        transaction.set_dup_format()
        for dup_format in transaction.dup_formats:
            if dup_format not in dup_cnt:
                dup_cnt[dup_format] = 0
            cnt = dup_cnt[dup_format]
            cnt +=1
            dup_cnt[dup_format] = cnt
        if "other" in transaction.dup_formats or 'last_line' in transaction.dup_formats:
            transaction.print()
    logger.info(f' num of transactions -----> {len(transactions)}')
    for dup_format in dup_cnt.keys():
        logger.info (f' dup count {dup_format} - {dup_cnt[dup_format]}')


load_recs(log1)
load_recs(log2)