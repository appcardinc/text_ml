from multiprocessing import Process
import os

def info(title):
    print(title)
    print('module name:', __name__)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())


def f(name, s1):
    info('function f')
    print('hello', name, s1)

if __name__ == '__main__':
    info('main line')
    p = Process(target=f, args=('bob','k1'))
    p.start()
    p.join()