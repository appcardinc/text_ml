import logging

from a_main import file_path
logging.basicConfig(handlers=[logging.FileHandler(file_path.path_log),
                              logging.StreamHandler()],level=logging.DEBUG)
logger = logging.getLogger('text_analysis')


def get_logger():
    return logger