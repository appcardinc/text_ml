import json
import re
from pathlib import PurePath
import logging
import random
from Text_Mining.wordnet.single_noun import *
from Text_Mining.file_path.file_path_new import *

products_count = 0
tagged_count = 0
min_confidence = 8
train_test = []
work_data = []
train_factor = 0.9
with open(path_tagged_products, 'r') as products_file:
    for rec in products_file:
        products_count += 1
        dic = []
        if products_count % 10_000 == 0:
            logging.info(products_count)
        try:
            dic = json.loads(rec)
        except:
            logging.warning(f'json error {products_count} -  {rec}')
            continue
        if 'main_concept' not in dic:
            main_concept = '***unk'
        else:
            main_concept = dic['main_concept']
        if main_concept.strip() == '':
            main_concept = '***unk'
        if main_concept != '***unk':
            tagged_count += 1
        item_name = dic['item_name'].lower()
        item_name = get_single_noun(item_name)

        item_name = re.sub('\\d', '#', item_name)
        confidence = dic['main_concept_confidence']
        rec = '__label__' + main_concept + ' ' + item_name
        if confidence > min_confidence and main_concept != '***unk':
            train_test.append(rec)
        else:
            work_data.append(rec)
print(f'products: {products_count} tagged: {tagged_count} train+test: {len(train_test)} work:  {len(work_data)} ')

path_product_data = PurePath(path_fastText, 'products_data')
path_train = PurePath(path_product_data, 'train.txt')
path_test = PurePath(path_product_data, 'validate.txt')
path_work = PurePath(path_product_data, 'work.txt')

train_cnt = 0
test_cnt = 0
work_cnt = 0

with open(path_train, 'w',encoding='utf8') as train, open(path_test, 'w',encoding='utf8') as test:
    for rec in train_test:
        s = random.uniform(0, 1)
        if s < train_factor:
            train.write(rec+'\n')
            train_cnt += 1
        else:
            test.write(rec + '\n')
            test_cnt += 1
print(f'train: {train_cnt}  test {test_cnt}')

with open(path_work, 'w',encoding='utf8') as work:
    for rec in work_data:
        work.write(rec + '\n')
        work_cnt += 1
print(f'work: {work_cnt}')