#!/usr/bin/env python

# Copyright (c) 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division, absolute_import, print_function

import os

from fastText import  train_supervised
from Text_Mining.file_path.file_path_new import *
from Text_Mining.utils.tm_log import *

path_product_data = os.path.join(path_fastText, 'products_data')
path_train = os.path.join(path_product_data, 'train.txt')
path_validate = os.path.join(path_product_data, 'validate.txt')
path_work = os.path.join(path_product_data, 'work.txt')
logger = init_tm_log()
logger.info('Start')


def print_results(N, p, r):
    print("N\t" + str(N))
    print("P@{}\t{:.3f}".format(1, p))
    print("R@{}\t{:.3f}".format(1, r))


def train_model():

    # train_supervised uses the same arguments and defaults as the testFastText cli
    model = train_supervised(
        input=path_train, epoch=40, lr=1.0, wordNgrams=3, verbose=2, minCount=1
    )

    text = 'hog wash piggy pink lemonade'
    
    s = model.predict (text , k=3 , threshold=0)
    print(text)
    print(s)

    print_results(*model.test(path_validate))
    path_model = os.path.join(path_product_data, 'products.bin')
    model.save_model(path_model)
    return model


def test_model(model):
    cnt = 0
    with open(path_work , 'r') as work:
        for rec in work:
            rec = rec.replace ('\n' , '')
            cnt += 1
            a = rec.split (' ',1)
            print (a)
            label = a[0].replace ('__label__' , '')
            name = a[1]
            s = model.predict (name , k=3 , threshold=0)
            logger.info(name + ' - ' + ' label:' + label + ' -  ' + str (s))
            if cnt > 3000:
                break


if __name__ == "__main__":
    model = train_model()
    test_model(model)
