#!/usr/bin/env python

# Copyright (c) 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division, absolute_import, print_function

import os
from fastText import train_supervised
from Text_Mining.file_path.file_path_new import *


def print_results(N, p, r):
    print("N\t" + str(N))
    print("P@{}\t{:.3f}".format(1, p))
    print("R@{}\t{:.3f}".format(1, r))


if __name__ == "__main__":
    path_product_data = os.path.join(path_fastText, 'products_data')
    path_train = os.path.join(path_product_data, 'train.txt')
    path_test = os.path.join(path_product_data, 'test.txt')


    # train_supervised uses the same arguments and defaults as the testFastText cli
    model = train_supervised(
        input=path_train, epoch=25, lr=1.0, wordNgrams=2, verbose=2, minCount=1
    )

    text = 'hog wash piggy pink lemonade'

    s = model.predict (text , k=3 , threshold=0)
    print(text)
    print(s)

    print_results(*model.test(path_test))

    print('train 2')

    model = train_supervised(
        input=path_train, epoch=25, lr=1.0, wordNgrams=2, verbose=2, minCount=1,
        loss="hs"
    )

    print_results(*model.test(path_test))
    path_model = os.path.join(path_product_data, 'products.bin')
    model.save_model(path_model)

    model.quantize(input=path_train, qnorm=True, retrain=True, cutoff=100000)
    print_results(*model.test(path_test))
    path_model = os.path.join(path_product_data, 'cooking.ftx')
    model.save_model(path_model)