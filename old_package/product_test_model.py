#!/usr/bin/env python

# Copyright (c) 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division, absolute_import, print_function

import fastText
import os
from Text_Mining.file_path import file_path_new
from Text_Mining.utils import tm_log

path_product_data = os.path.join(file_path_new.path_fastText, 'products_data')
path_work = os.path.join(path_product_data, 'work.txt')
path_model = os.path.join(path_product_data, 'products.bin')
logger = tm_log.get_log()
logger.info('Start')


def test_model(model):
    cnt_unk = 0
    cnt = 0
    cnt_dif = 0
    cnt_dif_high = 0
    min_prob = 0.85
    with open(path_work , 'r') as work:
        for rec in work:
            rec = rec.replace ('\n' , '')
            cnt += 1
            a = rec.split (' ',1)
            label = a[0].replace ('__label__' , '')
            cnt_unk += 1
            name = a[1]
            s = model.predict (name, k=3, threshold=0)
            s_label = s[0][0].replace ('__label__' , '')
            s_prob = s[1][0]
            if s_label != label:
                cnt_dif += 1
                if s_prob > min_prob:
                    cnt_dif_high += 1
                    fp = '{:.2f}'.format(s_prob)
                    logger.info(f' dif label: {name}  old: {label} new: {s_label} prob: {fp} s:--:  {s}')
    print (f' work: {cnt} dif: {cnt_dif}  high: {cnt_dif_high}')


if __name__ == "__main__":
    model = fastText.load_model(path_model)
    test_model(model)
