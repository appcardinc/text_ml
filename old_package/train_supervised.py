#!/usr/bin/env python

# Copyright (c) 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division, absolute_import, print_function

import os
from fastText import train_supervised


def print_results(N, p, r):
    print("N\t" + str(N))
    print("P@{}\t{:.3f}".format(1, p))
    print("R@{}\t{:.3f}".format(1, r))


if __name__ == "__main__":
    path = '/Users/ittai/fastText'
    train_data = os.path.join(path,  'cooking.train')
    valid_data = os.path.join(path,  'cooking.valid')
    models_path = os.path.join(path,  'models')
    # train_supervised uses the same arguments and defaults as the testFastText cli
    model = train_supervised(
        input=train_data, epoch=25, lr=1.0, wordNgrams=2, verbose=2, minCount=1
    )

    text = 'How to peel chestnuts?'

    s = model.predict (text , k=3 , threshold=0)
    print(text)
    print(s)

    print_results(*model.test(valid_data))

    print('train 2')

    model = train_supervised(
        input=train_data, epoch=25, lr=1.0, wordNgrams=2, verbose=2, minCount=1,
        loss="hs"
    )
    print_results(*model.test(valid_data))
    path_model = os.path.join(models_path, 'cooking.bin')
    model.save_model(path_model)

    model.quantize(input=train_data, qnorm=True, retrain=True, cutoff=100000)
    print_results(*model.test(valid_data))
    path_model = os.path.join(models_path, 'cooking.ftx')
    model.save_model(path_model)
